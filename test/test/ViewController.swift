//
//  ViewController.swift
//  test
//
//  Created by CGT on 24/07/18.
//  Copyright © 2018 cgt. All rights reserved.
//

import UIKit
import CoreData
class ViewController: UIViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet var password: UITextField!
    @IBOutlet var username: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        let context = appDelegate.persistentContainer.viewContext
//        let entity = NSEntityDescription.entity(forEntityName: "Users", in: context)
//        let newUser = NSManagedObject(entity: entity!, insertInto: context)
//        newUser.setValue("vishnu", forKey: "username")
//        newUser.setValue("123", forKey: "password")
//        do {
//            try context.save()
//        } catch {
//            print("Failed saving")
//        }
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
//        //request.predicate = NSPredicate(format: "age = %@", "12")
//        request.returnsObjectsAsFaults = false
//        do {
//            let result = try context.fetch(request)
//            for data in result as! [NSManagedObject] {
//                print(data.value(forKey: "username") as! String)
//                print(data)
//            }
//            
//        } catch {
//            
//            print("Failed")
//        }
//        
        
    }
    @IBAction func signup(_ sender: UIButton) {
        
        
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Users", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        
        newUser.setValue("\(String(describing: username.text))", forKey: "username")
        newUser.setValue("\(String(describing: password.text))", forKey: "password")
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
     
        
        
    }
    
    @IBAction func signin(_ sender: UIButton) {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "username = %@", "\(String(describing: username.text))")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                print(data.value(forKey: "username") as! String)
                print(data)
            }
            
        } catch {
            
            print("Failed")
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

