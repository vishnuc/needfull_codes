

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit

class ViewController: UIViewController,FBSDKLoginButtonDelegate {
    
    
    @IBOutlet weak var userFullName: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    

    @IBOutlet var login_btn: FBSDKLoginButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        login_btn.delegate = self
        login_btn.readPermissions = ["public_profile", "email"]
        if let _ = FBSDKAccessToken.current(){
            //already logged in
            fetchProfile()
        }
        
        //FBSDKLoginManagerLoginResult
        
        
//        userFullName.text = ""
//        
//        if let _ = FBSDKAccessToken.current()
//        {
//            fetchUserProfile()
//        }
        
        
        
        
        
        
    }
    func fetchProfile() {
        let parameters = ["fields": "first_name, email, last_name, picture"]
        
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (connection, user, requestError) -> Void in
            
            if requestError != nil {
                print("----------ERROR-----------")
                print(requestError)
                return
            }
            let userData = user as! NSDictionary
            let email = userData["email"] as? String
            let firstName = userData["first_name"] as? String
            self.userFullName.text = firstName
            let lastName = userData["last_name"] as? String
            var pictureUrl = ""
            if let picture = userData["picture"] as? NSDictionary, let data = picture["data"] as? NSDictionary, let url = data["url"] as? String {
                pictureUrl = url
                print(pictureUrl)
                
                var url_pic = URL(string: pictureUrl)

                            DispatchQueue.global(qos: .background).async {

                                DispatchQueue.main.async {

                                    let imageData = NSData(contentsOf: url_pic!)

                               DispatchQueue.main.async(execute: { () -> Void in
                                    if let imageData = imageData
                                    {
                                        let userProfileImage = UIImage(data: imageData as Data)
                                        self.userProfileImage.image = userProfileImage
                                        self.userProfileImage.contentMode = UIView.ContentMode.scaleAspectFit
                                    }
                                })
                            }
                        }
                
                
                
                
                
                
            }
        })
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("did logout of facebook")
        userFullName.text = ""
        userProfileImage.image = UIImage()
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil{
            print(error)
            return
        }
        print("succccessfully logged in with facebook")
        print(result)
        
        fetchProfile()

        
        
        
//
//        if let userName = result.value(forKey: "name") as? String
//        {
//            self.userFullName.text = userName
//        }
//
//
//        if let profilePictureObj = result.value(forKey: "picture") as? NSDictionary
//        {
//            let data = profilePictureObj.value(forKey: "data") as! NSDictionary
//            let pictureUrlString  = data.value(forKey: "url") as! String
//            let pictureUrl = NSURL(string: pictureUrlString)
//
//            DispatchQueue.global(qos: .background).async {
//
//                DispatchQueue.main.async {
//
//                    let imageData = NSData(contentsOf: pictureUrl! as URL)
//
//               DispatchQueue.main.async(execute: { () -> Void in
//                    if let imageData = imageData
//                    {
//                        let userProfileImage = UIImage(data: imageData as Data)
//                        self.userProfileImage.image = userProfileImage
//                        self.userProfileImage.contentMode = UIView.ContentMode.scaleAspectFit
//                    }
//                })
//            }
//        }
//        }
        
        
        
        
    }
    
    
    
    
    func fetchUserProfile()
    {
//        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, picture.width(480).height(480)"])
//
//        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
//
//            if ((error) != nil)
//            {
//                print("Error took place: \(error)")
//            }
//            else
//            {
//                print("Print entire fetched result: \(result)")
//
//                let id : NSString = (result as! Dictionary<String,Any>).values() as! String
//                print("User ID is: \(id)")
//
//                if let userName = result.valueForKey("name") as? String
//                {
//                    self.userFullName.text = userName
//                }
//
//                if let profilePictureObj = result.valueForKey("picture") as? NSDictionary
//                {
//                    let data = profilePictureObj.valueForKey("data") as! NSDictionary
//                    let pictureUrlString  = data.valueForKey("url") as! String
//                    let pictureUrl = NSURL(string: pictureUrlString)
//
//                    DispatchQueue.global(qos: .background).async {
//
//                        DispatchQueue.main.async {
//
//                            let imageData = NSData(contentsOf: pictureUrl! as URL)
//
//                            DispatchQueue.main.async(execute: { () -> Void in
//                                if let imageData = imageData
//                                {
//                                    let userProfileImage = UIImage(data: imageData as Data)
//                                    self.userProfileImage.image = userProfileImage
//                                    self.userProfileImage.contentMode = UIView.ContentMode.scaleAspectFit
//                                }
//                            })
//                        }
//                    }
//                }
//            }
//        })
    }
    
    
    
    

}

