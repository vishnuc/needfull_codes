//
//  cake_designing_image_fixing.swift
//  LaBrioche
//
//  Created by CGT on 28/08/18.
//  Copyright © 2018 cgt. All rights reserved.
//

import UIKit

class cake_designing_image_fixing: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet var cake_img: UIImageView!
    var identity = CGAffineTransform.identity
    var location = CGPoint(x: 0, y: 0)

    @IBOutlet var img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        img.center = cake_img.center
        
        
        
       // view.backgroundColor = .white
       // setupViews()
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(scale))
        let rotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotate))
        
        pinchGesture.delegate = self
        rotationGesture.delegate = self
        
        img.addGestureRecognizer(pinchGesture)
        img.addGestureRecognizer(rotationGesture)
        
        
        
        
    }
    
//    let firstImageView: DraggableImageView = {
//        let iv = DraggableImageView()
//        iv.backgroundColor = .red
//        iv.isUserInteractionEnabled = true
//        return iv
//    }()
//
//    func setupViews() {
//        view.addSubview(firstImageView)
//        let firstImageWidth: CGFloat = 50
//        let firstImageHeight: CGFloat = 50
//        firstImageView.frame = CGRect(x: view.frame.midX - firstImageWidth / 2, y: view.frame.midY - firstImageWidth / 2, width: firstImageWidth, height: firstImageHeight)
//    }
    @objc func scale(_ gesture: UIPinchGestureRecognizer) {
        
        print("pinch")
        
        switch gesture.state {
        case .began:
            identity = img.transform
        case .changed,.ended:
            img.transform = identity.scaledBy(x: gesture.scale, y: gesture.scale)
        case .cancelled:
            break
        default:
            break
        }
    }
    @objc func rotate(_ gesture: UIRotationGestureRecognizer) {
        print("rotate")
        
        img.transform = img.transform.rotated(by: gesture.rotation)
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent!) {
        let touch : UITouch! = touches.first




        location = touch.location(in: view)
        img.center = location

    }
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let touch : UITouch! = touches.first
//
//
//
//
//        location = touch.location(in: view)
//        img.center = location
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}







class DraggableImageView: UIImageView {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        backgroundColor = .clear
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        backgroundColor = .clear
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let position = touches.first?.location(in: superview){
            center = position
        }
    }
}



