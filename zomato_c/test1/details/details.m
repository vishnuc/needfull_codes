//
//  details.m
//  test1
//
//  Created by CGT on 22/10/18.
//  Copyright © 2018 vishnu. All rights reserved.
//

#import "details.h"
#import "menu.h"
#import "rating.h"

@interface details (){
    
    NSNumber *id1 ;
    IBOutlet UIImageView *img;
    IBOutlet UILabel *rating_lbl;
    IBOutlet UILabel *name_lbl;
    IBOutlet UILabel *address1_lbl;
    IBOutlet UITextView *address2_lbl;
    
    NSDictionary *all_details;
}

@end

@implementation details
- (IBAction)daily_menu:(UIButton *)sender {
    
    
    
    [self performSegueWithIdentifier:@"dially" sender:_res_id];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"dially"]) {
        //NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        menu *destViewController = segue.destinationViewController;
        destViewController.res_id = sender;
    }
    if ([segue.identifier isEqualToString:@"review"]) {
        //NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        rating *destViewController = segue.destinationViewController;
        destViewController.res_id = sender;
    }
}
- (IBAction)rating:(id)sender {
   [self performSegueWithIdentifier:@"review" sender:_res_id];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"re id %@",self.res_id);
    
    
    
    
    
    
    
    
    
    __block NSError *error1 = [[NSError alloc] init];
    __block NSMutableDictionary *resultsDictionary;
    
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    
    NSString *ur = [NSString stringWithFormat:@"https://developers.zomato.com/api/v2.1/restaurant?res_id=%@",self.res_id];
    
    NSURL *url = [NSURL URLWithString:ur];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request setValue:@"3af8d2cc4cbb277edeccb34b2cb787ce" forHTTPHeaderField:@"user-key"];
    [request setValue:[NSString stringWithFormat:@"%@",self.res_id] forHTTPHeaderField:@"res_id"];
    
    
    
    [request setHTTPMethod:@"GET"];
    //    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
    //                             @"IOS TYPE", @"typemap",
    //                             nil];
    //    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    //    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        
        
        if ([data length]>0 && error == nil) {
            resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
            NSLog(@"resultsDictionary is %@",resultsDictionary);
            
            
            if ([data length] > 0){
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    
                    
                self->all_details = [[NSDictionary alloc]init];
                self->all_details = resultsDictionary;
                self->name_lbl.text =  resultsDictionary[@"name"];
                
                NSDictionary *loc = resultsDictionary[@"location"];
                
                self->address1_lbl.text = loc[@"locality"];
                
                self->address2_lbl.text = loc[@"address"];
                
                NSDictionary *rating = resultsDictionary[@"user_rating"];
                
                    self->rating_lbl.text = rating[@"aggregate_rating"];
                    
                    
                    
                    
                    
                    NSURL *url = [NSURL URLWithString:
                                  resultsDictionary[@"thumb"]];
                    
                    // 2
                    NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                                   downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                                       // 3
                                                                       UIImage *downloadedImage = [UIImage imageWithData:
                                                                                                   [NSData dataWithContentsOfURL:location]];
                                                                       
                                                                       //5
                                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                                           self->img.image = downloadedImage;
                                                                       });
                                                                   }];
                    
                    // 4
                    [downloadPhotoTask resume];
                    
                    
                    
                    
                    
                    
                
                //[self.tab reloadData];
                //self.viewDidLoad;
                
                    
                });
                
                
            }
            
           
            
            
            
            
            
        } else if ([data length]==0 && error ==nil) {
            NSLog(@" download data is null");
            
         
            
        } else if( error!=nil) {
            NSLog(@" error is %@",error);
        }
        
        
        
    }];
    
    [postDataTask resume];
    
    
    
    
    
    
    
    
    
    
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
