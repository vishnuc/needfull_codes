////
////  MyManager.h
////  test1
////
////  Created by CGT on 22/10/18.
////  Copyright © 2018 vishnu. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//
//NS_ASSUME_NONNULL_BEGIN
//
//@interface MyManager : UIViewController
//
//@end
//
//NS_ASSUME_NONNULL_END
#import <foundation/Foundation.h>

@interface MyManager : NSObject {
    NSString *someProperty;
}

@property (nonatomic, retain) NSString *someProperty;

+ (id)sharedManager;

@end
