//
//  rating_cell.h
//  test1
//
//  Created by CGT on 22/10/18.
//  Copyright © 2018 vishnu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface rating_cell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *rate;
@property (strong, nonatomic) IBOutlet UIImageView *img;

@end

NS_ASSUME_NONNULL_END
