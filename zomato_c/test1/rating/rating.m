//
//  rating.m
//  test1
//
//  Created by CGT on 22/10/18.
//  Copyright © 2018 vishnu. All rights reserved.
//

#import "rating.h"
#import "rating_cell.h"

@interface rating ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *all_reviews;
    
    
}
@end

@implementation rating

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    self.tab.rowHeight = UITableViewAutomaticDimension;
    
    
    
    
    
    __block NSError *error1 = [[NSError alloc] init];
    __block NSMutableDictionary *resultsDictionary;
    
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    
    NSString *ur = [NSString stringWithFormat:@"https://developers.zomato.com/api/v2.1/reviews?res_id=%@",self.res_id];
    
    NSURL *url = [NSURL URLWithString:ur];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request setValue:@"3af8d2cc4cbb277edeccb34b2cb787ce" forHTTPHeaderField:@"user-key"];
    [request setValue:[NSString stringWithFormat:@"%@",self.res_id] forHTTPHeaderField:@"res_id"];
    
    
    
    [request setHTTPMethod:@"GET"];
    //    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: @"TEST IOS", @"name",
    //                             @"IOS TYPE", @"typemap",
    //                             nil];
    //    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    //    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        
        
        if ([data length]>0 && error == nil) {
            resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
            NSLog(@"resultsDictionary is %@",resultsDictionary);
            
            
            if ([data length] > 0){
                
                
                
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    
                    
                    
                    if (resultsDictionary[@"reviews_count"] == 0 ){
                        
                        UIAlertController * alert = [UIAlertController
                                                     alertControllerWithTitle:@""
                                                     message:[NSString stringWithFormat:@"No reviews found "]
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        
                        //Add Buttons
                        
                        UIAlertAction* yesButton = [UIAlertAction
                                                    actionWithTitle:@"Ok"
                                                    style:UIAlertActionStyleDefault
                                                    handler:nil];
                        
                        
                        [alert addAction:yesButton];
                        
                        
                        [self presentViewController:alert animated:YES completion:nil];
                        
                        
                    }
                    else{
                        self->all_reviews = resultsDictionary[@"user_reviews"];
                        
                        
                        [self.tab reloadData];
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                });
                
                
                
                
                
            }
            
            
            
            
        } else if ([data length]==0 && error ==nil) {
            NSLog(@" download data is null");
            
            
            if ([data length] > 0){
                
                //                                self->allhotels = [[NSArray alloc]init];
                //                                self->allhotels = resultsDictionary[@"nearby_restaurants"];
                //                                [self.tab reloadData];
                
                
            }
            
            
            
            
            
            
        } else if( error!=nil) {
            NSLog(@" error is %@",error);
        }
        
        
        
    }];
    
    [postDataTask resume];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self->all_reviews.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"cell";
    
    rating_cell *cell = (rating_cell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"cell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.img.layer.cornerRadius = 30;
    
    NSDictionary *all = self->all_reviews[indexPath.row];
    
    NSDictionary *review = all[@"review"];
    
    cell.rate.text = review[@"review_text"];
    NSDictionary *user = review[@"user"];
    cell.name.text = user[@"name"];
    
    self.tab.rowHeight = UITableViewAutomaticDimension;
    
    
   NSString *img = user[@"profile_image"];
    
    NSURL *url = [NSURL URLWithString:
                  img];
    
    // 2
    NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                   downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                       // 3
                                                       UIImage *downloadedImage = [UIImage imageWithData:
                                                                                   [NSData dataWithContentsOfURL:location]];
                                                       
                                                       //5
                                                       dispatch_async(dispatch_get_main_queue(), ^{
                                                           cell.img.image = downloadedImage;
                                                       });
                                                   }];
    
    // 4
    [downloadPhotoTask resume];
    
    
    
    
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
