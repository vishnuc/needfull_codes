//
//  Restuarants_cell.h
//  test1
//
//  Created by CGT on 22/10/18.
//  Copyright © 2018 vishnu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Restuarants_cell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *banner_img;
@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UILabel *lbl;
@property (strong, nonatomic) IBOutlet UIButton *website_btn;

@end

NS_ASSUME_NONNULL_END
