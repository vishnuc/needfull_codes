//
//  rear_vc.m
//  test1
//
//  Created by CGT on 22/10/18.
//  Copyright © 2018 vishnu. All rights reserved.
//

#import "rear_vc.h"
#import "rear_vc.h"
#import <CoreLocation/CoreLocation.h>

@interface rear_vc ()<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    NSString *lon;
    NSString *lat;
}
@end

@implementation rear_vc

- (IBAction)mylocation:(id)sender {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"My location details are"
                                 message:[NSString stringWithFormat:@"longitude:%@,latitude:%@",lon,lat]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:nil];
    
    
    [alert addAction:yesButton];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)help:(id)sender {
}
- (IBAction)about:(id)sender {
}

-(void)viewDidAppear:(BOOL)animated{
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    locationManager.startUpdatingLocation;
    
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"failed to fetch current location : %@", error);
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    NSLog(@"my location::::::::::::::::::::%f",locationManager.location.coordinate.latitude);
    //NSString *lon = (@"%f",locationManager.location.coordinate.longitude);
    lon = [[NSNumber numberWithFloat:locationManager.location.coordinate.longitude] stringValue];
    lat = [[NSNumber numberWithFloat:locationManager.location.coordinate.latitude] stringValue];
    NSLog(@"my location::::::::::::::::::::%@",lat);
    // locationManager.location.coordinate.longitude
    
    [locationManager stopUpdatingLocation];
    
    
    
    
    
    
    
    
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _text_lbl.layer.cornerRadius = 35;
    
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    NSString *namekey = @"username";
    
    if ([preferences objectForKey:namekey] == nil)
    {
        //  Doesn't exist.
    }
    else
    {
        //  Get name
        const NSString *name = [preferences objectForKey:namekey];
        self.name_lbl.text =  name;
        NSString *firstLetter = [name substringToIndex:1];
        self.text_lbl.text = firstLetter;
        
        
        
    }
    
    
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
