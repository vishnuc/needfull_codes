
import UIKit
import Alamofire
import AlamofireImage

class sub_bar_first_item: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    @IBOutlet var search_btn: UIButton!
    @IBOutlet var search_txt: UITextField!
    @IBOutlet var tab: UITableView!
    let imageCache = NSCache<NSString, UIImage>()
    var json_data = NSArray()
    var search_status = false
    var search_result = NSMutableArray()


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tab.delegate = self
        tab.dataSource = self
        
        
        tab.rowHeight = UITableViewAutomaticDimension
        search_txt.delegate=self
        
        search_btn.addTarget(self, action: #selector(action), for: .touchUpInside)
        
        
        authentication_services.instance.product_list(){ (success) in
            
            
            if success {
                
                
                var d1 :Dictionary<String,Any> = userdata_services.instance.json_data["data"] as! Dictionary
                print(d1)
                var d2 = d1["product_categories"] as! Dictionary<String,Any>
                let d3:NSArray = d2["data"] as! NSArray
                self.json_data = d3
                self.tab.reloadData()
                
                
            }
            else{
                print("network problem")
            }
            
            
            
            
            
        }

        
    }

  //delegate_methords
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        action()
        return true
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if search_status==true {
            return search_result.count
        }
            
        else{ return json_data.count }

    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let v = UIView()
//        v.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//        return v
//    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! sub_bar_first_item_cell
        if search_status == false {
            
            cell.img.layer.cornerRadius = cell.img.frame.height/2
            cell.img.clipsToBounds = true
            cell.lbl1.text =  (((json_data.value(forKey: "product_categories_detail_name") as! NSArray).object(at: indexPath.section)) as? String)
            cell.lbl2.text = (((json_data.value(forKey: "product_category_status") as! NSArray).object(at: indexPath.section)) as? String)
            
            cell.lbl3.text = (((json_data.value(forKey: "product_categories_detail_updated_at") as! NSArray).object(at: indexPath.section)) as? String)
            
            let img_name = (((json_data.value(forKey: "product_category_image") as! NSArray).object(at: indexPath.section)) as! String)
            
            
            if let cacheImage = imageCache.object(forKey: img_name as NSString ){
                print("image cache")
                cell.img.image = cacheImage
                //return
            }
            else{
                print("image not cache")
                
                print("\(IMG_URL)\(String(describing: img_name))")
                
                Alamofire.request("\(IMG_URL)\(String(describing: img_name))").responseImage { response in
                    //debugPrint(response)
                    
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        cell.img.image = image
                        self.imageCache.setObject(image, forKey: img_name as NSString)
                        //      }
                    }
                    
                }
            }
            
        }
        else{
            
            
            
            
            cell.img.layer.cornerRadius = cell.img.frame.height/2
            cell.img.clipsToBounds = true
            
            
            let s : NSArray = search_result as NSArray
            
            cell.lbl1.text =  (((s.value(forKey: "product_categories_detail_name") as! NSArray).object(at: indexPath.section)) as? String)
            cell.lbl2.text = (((s.value(forKey: "product_category_status") as! NSArray).object(at: indexPath.section)) as? String)
            
            cell.lbl3.text = (((s.value(forKey: "product_categories_detail_updated_at") as! NSArray).object(at: indexPath.section)) as? String)
            
            let img_name = (((s.value(forKey: "product_category_image") as! NSArray).object(at: indexPath.section)) as! String)
            
            
            if let cacheImage = imageCache.object(forKey: img_name as NSString ){
                print("image cache")
                cell.img.image = cacheImage
                //return
            }
            else{
                print("image not cache")
                
                print("\(IMG_URL)\(String(describing: img_name))")
                
                Alamofire.request("\(IMG_URL)\(String(describing: img_name))").responseImage { response in
                    //debugPrint(response)
                    
                    //print(response.request)
                    //print(response.response)
                    //debugPrint(response.result)
                    
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        cell.img.image = image
                        self.imageCache.setObject(image, forKey: img_name as NSString)
                        //      }
                    }
                    
                }
            }
            
            
            
            
            
            
            
            
            
        }

        if indexPath.section == 0 {
            tableView.tableHeaderView?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        else{
            tableView.tableHeaderView?.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.7372377997)
        }
        return cell
    }
    @IBAction func action() {
        search_result = NSMutableArray()
        view.endEditing(true)
        if search_txt.text == ""
        {
            self.search_status = false
            self.tab.reloadData()
        }
        else{
            search_status = true
            
            //var i = 0
            var j = 0
            
            
            for i in 0...json_data.count-1{
                
                if (((json_data.value(forKey: "product_categories_detail_name") as! NSArray).object(at: i)) as? String)?.lowercased().range(of:search_txt.text!) != nil {
                    print("exists")
                    
                    self.search_result[j] = self.json_data[i]
                    j = j+1
                    
                    
                }
                self.tab.reloadData()
            }
        }
        
    }

    


}
