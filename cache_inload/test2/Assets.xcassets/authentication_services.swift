
import Foundation
import Alamofire


class authentication_services {
    static let instance = authentication_services()
    
    let defaults = UserDefaults.standard
    
    
    
    var isloggedin : Bool{
        
        get{
            
            return defaults.value(forKey: "isloggedin") as! Bool
            
        }
        set{
            
            defaults.set(newValue, forKey: "isloggedin")
            
        }
        
        
    }
    
    
    
    
    func product_list(completion:@escaping CompletionHandler )  {
        let body: [String: Any] = [
            "language_id": "1"
            
        ]
        
        
        Alamofire.request(BASE_URL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON{(response) in
            
            if response.result.error == nil {
                guard let response_data = response.data else {return}
                
                let json = try? JSONSerialization.jsonObject(with: response_data, options: []) as Any
                print(json as Any)
                userdata_services.instance.json_data = json as! [String : Any]
                completion(true)
                
            }
            else{
                
                
                
                
                
                completion(false)
            }
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
}

