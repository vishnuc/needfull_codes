//
//  ViewController.swift
//  test2
//
//  Created by CGT on 01/08/18.
//  Copyright © 2018 cgt. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

let imageCache = NSCache<NSString, UIImage>()

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TableViewCell
        
//        if cell?.img == nil{
       var  imageUrl = "https://httpbin.org/image/png"
        if indexPath.row % 2 == 0{
            
            imageUrl = "https://duckduckgo.com/i/fd2dabcf.jpg"
        }
        
        if let cacheImage = imageCache.object(forKey: imageUrl as NSString){
            print("image cache")
            cell?.img.image = cacheImage
            //return
        }
        else{
        print("image not cache")
        
        
        
        Alamofire.request(imageUrl).responseImage { response in
            //debugPrint(response)
            
            //print(response.request)
            //print(response.response)
            //debugPrint(response.result)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
                cell?.img.image = image
                imageCache.setObject(image, forKey: imageUrl as NSString)
      //      }
            }

        }
        }
        
        
        
        if indexPath.row == 9 {
            
            print(imageCache)
            
        }
        
        
        
        return cell!
    }
    
    @IBOutlet var tab: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tab.delegate = self
        tab.dataSource = self
        tab.rowHeight = 94
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
class TableViewCell2: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
