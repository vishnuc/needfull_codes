//
//  collection_vc.swift
//  test2
//
//  Created by CGT on 13/08/18.
//  Copyright © 2018 cgt. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class collection_vc: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    
   let imageCache = NSCache<NSString, UIImage>()
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 150
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        //        if cell?.img == nil{
        var  imageUrl = "https://httpbin.org/image/png"
        if indexPath.row % 2 == 0{
            
            imageUrl = "https://duckduckgo.com/i/fd2dabcf.jpg"
        }
        
        if let cacheImage = imageCache.object(forKey: imageUrl as NSString){
            print("image cache")
            cell.img.image = cacheImage
            //return
        }
        else{
            print("image not cache")
        
            
            
            Alamofire.request(imageUrl).responseImage { response in
                //debugPrint(response)
                
                //print(response.request)
                //print(response.response)
                //debugPrint(response.result)
                
                if let image = response.result.value {
                    print("image downloaded: \(image)")
                    cell.img.image = image
                    self.imageCache.setObject(image, forKey: imageUrl as NSString)
                    //      }
                }
                
            }
       }
        
        return cell
    }
    

    @IBOutlet var collection: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collection.delegate=self
        collection.dataSource=self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
