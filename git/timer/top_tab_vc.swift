
import UIKit
import XLPagerTabStrip

class top_tab_vc: ButtonBarPagerTabStripViewController {

    let purpleInspireColor = UIColor(red:0.13, green:0.03, blue:0.25, alpha:1.0)
    override func viewDidLoad() {
        // change selected bar color
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = purpleInspireColor
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 11)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        //settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        settings.style.buttonBarMinimumInteritemSpacing = 0
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .black
            oldCell?.imageView.contentMode = .scaleAspectFit
            newCell?.label.textColor = self?.purpleInspireColor
            
            newCell?.imageView.contentMode = .scaleAspectFit
            
        }
        super.viewDidLoad()
    }
    
    
    @IBAction func item1(_ sender: Any) {
        moveToViewController(at: 0, animated: true)
    }
    @IBAction func item2(_ sender: Any) {
        moveToViewController(at: 1, animated: true)
    }
    
    @IBAction func item3(_ sender: Any) {
        moveToViewController(at: 2, animated: true)
    }
    @IBAction func item4(_ sender: Any) {
        moveToViewController(at: 3, animated: true)
    }
    
    
    
    
    
    
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child1")
        let child_2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child2")
        let child_3 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child3")
        let child_4 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child4")
        
        
        
        return [child_1, child_2, child_3, child_4]
    }



}
