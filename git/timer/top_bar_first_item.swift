
import UIKit
import XLPagerTabStrip

class top_bar_first_item: UIViewController,IndicatorInfoProvider {

    @IBOutlet var lbl: UILabel!
    @IBOutlet var container: UIView!
    @IBOutlet var segment: UISegmentedControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segment_action(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            container.isHidden = false
            lbl.text = ""
        case 1:
            container.isHidden = true
            lbl.text = "Second"
            
        default:
            container.isHidden = true
            lbl.text = "Third"
        }
        
        
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        return IndicatorInfo(title: "Item 1")
    }
    


}
