//
//  home.swift
//  Sherazi
//
//  Created by CGT on 08/01/18.
//  Copyright © 2018 CGT. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import ReachabilitySwift
//import SystemConfiguration

class home: UIViewController ,UITableViewDataSource,UITableViewDelegate,NetworkStatusListener{
    @IBOutlet var dogs_btn: UIButton!
    @IBOutlet var menu: UIButton!
    
    @IBOutlet var spinner: UIActivityIndicatorView!
    @IBOutlet var home_tab: UITableView!
    @IBOutlet var cat_btn: UIButton!
    
    @IBOutlet var cart: UIButton!
    
    @IBOutlet var nw_lbl: UILabel!
    @IBOutlet var cart_nav: UIBarButtonItem!
    
    
    @IBAction func order_view(_ sender: Any) {
        if AuthService.instance.isLoggedIn==true{
        
            performSegue(withIdentifier: "toorder", sender: nil)}
        else{
            self.navigationController?.isNavigationBarHidden=true
            performSegue(withIdentifier: "tologin", sender: nil)
            
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
         ReachabilityManager.shared.addListener(listener: self)
        print("is nw available : \(ReachabilityManager.shared.isNetworkAvailable)")
        if ReachabilityManager.shared.isNetworkAvailable{
            
            
            print("is nw available : \(ReachabilityManager.shared.isNetworkAvailable)")
            
            
        }
        else{
            nw_lbl.isHidden=false
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserDataService.instance.home_items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? home_cell {
            
            cell.lbl.text=((UserDataService.instance.home_items.value(forKey: "product_categories_detail_name") as! NSArray).object(at: indexPath.row)) as? String
            
            
            if let im = ((UserDataService.instance.home_items.value(forKey: "product_category_image") as! NSArray).object(at: indexPath.row)) as? NSNull{}
            else {
            
            var image_name=((UserDataService.instance.home_items.value(forKey: "product_category_image") as! NSArray).object(at: indexPath.row)) as! String
            
            
            cell.img.layer.masksToBounds = false
            
            cell.img.layer.cornerRadius = cell.img.frame.height/2
            cell.img.clipsToBounds = true
            
            
            Alamofire.request("\(PRODUCT_CATEGORY_IMG_URL)\(image_name)",  method: .get).responseImage
                { response in
                    guard let image = response.result.value else {
                        // Handle error
                        print("image error")
                        print("\(BRAND_IMG_URL)\(image_name)")
                        return
                    }
                    // Do stuff with your image
                    cell.img.image=image
                    cell.img.layer.shadowColor=UIColor.black.cgColor
                    cell.img.layer.backgroundColor=UIColor.black.cgColor
                    cell.img.layer.opacity=0.5
                    
                }}
            
            
            
            
            return cell}
        return home_cell()
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        UserDataService.instance.pet_id=(((UserDataService.instance.home_items.value(forKey: "product_sub_category_product_category_id") as! NSArray).object(at: indexPath.row)) as? String)!
        UserDataService.instance.title_nav=((UserDataService.instance.home_items.value(forKey: "product_categories_detail_name") as! NSArray).object(at: indexPath.row)) as? String as! NSString
        
          performSegue(withIdentifier: "tocateg", sender: nil)
        
        //print(UserDataService.instance.product_id)
      
      
    }
//    override func viewLayoutMarginsDidChange() {
//         print("reveal toggled")
//    }
//    
//    override func revealViewController() -> SWRevealViewController! {
//     
//        var rv = SWRevealViewController()
//        
//        
//        
//        
//    return rv
//        
//        
//    }
//    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if AuthService.instance.cart.count>0{
            
            
            
            
            //cart_nav.addBadge(number: AuthService.instance.cart.count)
            cart_nav.updateBadge(number: AuthService.instance.cart.count)
            
            
        }
        else{
            
            
            cart_nav.removeBadge()
        }
        
        
        ReachabilityManager.shared.addListener(listener: self)
        if ReachabilityManager.shared.isNetworkAvailable{
            
            
            print("is nw available : \(ReachabilityManager.shared.isNetworkAvailable)")
            nw_lbl.isHidden=true
            
        }
        else{
            
            
            print("is nw available : \(ReachabilityManager.shared.isNetworkAvailable)")
            nw_lbl.isHidden=false
        }
        
        
    //    ReachabilityManager.shared.addListener(listener: self)
      //  ReachabilityManager.shared.addListener(listener: self as! NetworkStatusListener)
        
         self.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.hidesBackButton=true
        
        
        print("reveal toggled")
        self.navigationController?.isNavigationBarHidden=false
      //  viewDidLoad()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       // ReachabilityManager.shared.removeListener(listener: self as! NetworkStatusListener)
        ReachabilityManager.shared.removeListener(listener: (self))
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        self.navigationController?.isNavigationBarHidden=true
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
        navigationController?.navigationBar.isHidden=true
    }
    @objc func pressed(sender: UIButton!) {
        
        
        
        
        
        
        if AuthService.instance.cart.count>0{
            performSegue(withIdentifier: "tocart", sender: nil)}
        else{
            
            if AuthService.instance.language == "english"{
                let alert = UIAlertController(title: "Cart empty.", message: "", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)}
            else{
                let alert = UIAlertController(title:"السلة فارغه‫!‬", message: "", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "\(UserDataService.instance.OK)", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
            }
     
        }
    }
    
    @objc func alert_view()  {
        
    }
    
    
    func networkStatusDidChange(status: Reachability.NetworkStatus) {
        
        switch status {
        case .notReachable:
            debugPrint("ViewController: Network became unreachable")
            nw_lbl.isHidden=false
           // home_tab.isUserInteractionEnabled=false
            
        case .reachableViaWiFi:
            debugPrint("ViewController: Network reachable through WiFi")
            nw_lbl.isHidden=true
            home_tab.isUserInteractionEnabled=true
            viewDidLoad()
        case .reachableViaWWAN:
            debugPrint("ViewController: Network reachable through Cellular Data")
            nw_lbl.isHidden=true
            home_tab.isUserInteractionEnabled=true
            viewDidLoad()
        }
    }

    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDataService.instance.home_items=[]
        
        ReachabilityManager.shared.addListener(listener: self)
    
        
        
        
//        if ReachabilityManager.shared.isNetworkAvailable == false{
//            
//            self.nw_lbl.isHidden=false
//            
//            
//        }
        if AuthService.instance.language=="arabic"{
            
            nw_lbl.text = "تاكد من شبكه الانترنت"
            
        }
            
        if ReachabilityManager.shared.isNetworkAvailable{
            
            
            print("is nw available : \(ReachabilityManager.shared.isNetworkAvailable)")
            
            
        }
        else{
            
            
            print("is nw available : \(ReachabilityManager.shared.isNetworkAvailable)")
            nw_lbl.isHidden=false
        }
        
        
      
     
       navigationController?.navigationBar.isHidden=false
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationItem.backBarButtonItem?.isEnabled = false
        navigationController?.navigationBar.isHidden=false
        
//
//        if AuthService.instance.language == "arabic"{
//
//            UIView.appearance().semanticContentAttribute = .forceRightToLeft
//            UIView.userInterfaceLayoutDirection(for: view.semanticContentAttribute, relativeTo: UIUserInterfaceLayoutDirection.leftToRight)
//
//
//        }
//        else{
//            UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            UIView.userInterfaceLayoutDirection(for: view.semanticContentAttribute, relativeTo: UIUserInterfaceLayoutDirection.rightToLeft)
//
//        }
//
        
       // cart_nav.addBadge(number: 22, withOffset: CGPoint(x: 0, y: 0) , andColor: UIColor.red, andFilled: false)
        
        //            let notificationButton = SSBadgeButton()
        //            notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        //            notificationButton.setImage(UIImage(named: "cart.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        //            notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        //            notificationButton.badge = "\(AuthService.instance.cart.count)"
        //
        //            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: notificationButton)
        
        
     //    UIView.userInterfaceLayoutDirection(for: view.semanticContentAttribute, relativeTo: UIUserInterfaceLayoutDirection.rightToLeft)
        
        cart.addTarget(self, action: #selector(pressed(sender:)), for: .touchUpInside)
        
        
        
        
        if AuthService.instance.cart.count>0{
            
            
            
            
            cart_nav.addBadge(number: AuthService.instance.cart.count)
            
            
            
        }
        else{
            
            
            cart_nav.removeBadge()
        }
        
        
        
        
        
        
        spinner.isHidden=false
        spinner.startAnimating()
       
        navigationItem.backBarButtonItem?.isEnabled=false
        self.navigationItem.backBarButtonItem?.width=0
        self.navigationItem.backBarButtonItem?.customView?.isHidden=true
        self.navigationItem.hidesBackButton=true
        self.navigationItem.setHidesBackButton(true, animated: false)
        //navigationItem.backBarButtonItem.is
         //UserDataService.instance.product_id="0"
         home_tab.separatorColor=UIColor.clear
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //dogs_btn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        //cat_btn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        menu.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.navigationItem.leftItemsSupplementBackButton=false
      //  self.navigationController?.isNavigationBarHidden=true
       
        
        
        
        if AuthService.instance.language == "arabic"{
            
            
//            UIView.appearance().semanticContentAttribute = .forceRightToLeft
//            UIView.userInterfaceLayoutDirection(for: view.semanticContentAttribute, relativeTo: UIUserInterfaceLayoutDirection.leftToRight)
           
            
                    if self.revealViewController() != nil {
                        menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for:.touchUpInside)
                        self.revealViewController().rightViewRevealWidth = self.view.frame.size.width - 60;
                       // self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
                       // self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                        
                        var swipeLeft = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.rightRevealToggle(_:)))
                        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
                        self.view.addGestureRecognizer(swipeLeft)
            
                    }
            
            
            
            
        }
        else{
            
//          
//            UIView.appearance().semanticContentAttribute = .forceLeftToRight
//            UIView.userInterfaceLayoutDirection(for: view.semanticContentAttribute, relativeTo: UIUserInterfaceLayoutDirection.rightToLeft)
            if self.revealViewController() != nil {
                menu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
                self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60;
                //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
               // self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                
                
                var swipeRight = UISwipeGestureRecognizer(target: self.revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)))
                swipeRight.direction = UISwipeGestureRecognizerDirection.right
                self.view.addGestureRecognizer(swipeRight)
                
            }
            
            
        }
        
        
        

       
        
        
        AuthService.instance.product() { (success) in
            if success {
                
                self.nw_lbl.isHidden=true
                self.home_tab.reloadData()
                self.spinner.isHidden=true
                self.spinner.startAnimating()
                
            }
            else{
               // sel
                self.spinner.isHidden=true
                self.spinner.startAnimating()
                //self.nw_lbl.isHidden=false
                if AuthService.instance.language == "english"{
                    let alert = UIAlertController(title: "Network Problem!", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)}
                else{
                    let alert = UIAlertController(title: "تاكد من شبكه الانترنت", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "\(UserDataService.instance.OK)", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                
                
                
            }
            
            
            
            
            
            
            
        }
        
        
      home_tab.delegate=self
        home_tab.dataSource=self
        
        
        
        
     // print(AuthService.instance.user_id)
    
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}







//extension ViewController: NetworkStatusListener {
//
//    func networkStatusDidChange(status: Reachability.NetworkStatus) {
//
//        switch status {
//        case .notReachable:
//            debugPrint("ViewController: Network became unreachable")
//        case .reachableViaWiFi:
//            debugPrint("ViewController: Network reachable through WiFi")
//        case .reachableViaWWAN:
//            debugPrint("ViewController: Network reachable through Cellular Data")
//        }
//    }
//}






