//
//  sw_rear_VC.swift
//  api_drawer
//
//  Created by CGT on 30/08/18.
//  Copyright © 2018 vishnu. All rights reserved.
//

import UIKit
import SWRevealViewController

class sw_rear_VC: UIViewController {
    
    var view1: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        

        
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        //Create the view the same size as your frontViewController
        view1 = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(revealViewController().view.frame.size.width), height: CGFloat(revealViewController().view.frame.size.height)))
        //Set the colour of the view to whatever you like
        view1?.backgroundColor = UIColor(white :1 ,alpha:0.5)
        view1?.backgroundColor = #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 0.5076787243)
        //Add a tap gesture to the UIview
        let tap = UITapGestureRecognizer(target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        view1?.addGestureRecognizer(tap)
        //Add the new view to the frontViewcontroller
        revealViewController().frontViewController.view.addSubview(view1!)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        view1?.removeFromSuperview()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
